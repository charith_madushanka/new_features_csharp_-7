# New Features c# 7

Tuples
It is common to want to return more than one value from a method. The options available today are less than optimal:

Out parameters: Use is clunky (even with the improvements described above), and they don�t work with async methods.
System.Tuple<...> return types: Verbose to use and require an allocation of a tuple object.
Custom-built transport type for every method: A lot of code overhead for a type whose purpose is just to temporarily group a few values.
Anonymous types returned through a dynamic return type: High performance overhead and no static type checking.
To do better at this, C# 7.0 adds tuple types and tuple literals:

```


		private static (int sum, int count,int avarage) GetSumCountTuples(int [] marks)
		{
					var result = (s: 0, c: 0,a:0);

					if (!(marks is null))
					{
						foreach (int mark in marks)
						{
							Add(mark, 1);
						}
                
						void Add(int s, int c) { result.s += s; result.c += c; result.a = result.s / result.c; };
					}

					return result;
		}


```

Local functions
Sometimes a helper function only makes sense inside of a single method that uses it. You can now declare such functions inside other function bodies as a local function:

```

	 if (!(marks is null))
	 {
					foreach (int mark in marks)
					{
						Add(mark, 1);
					}
                
					void Add(int s, int c) { result.s += s; result.c += c; result.a = result.s / result.c; };
	 }

```