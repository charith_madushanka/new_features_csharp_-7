﻿using System;

namespace CSharp7
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int[] marks = { 0b10100, 0b1100100, 0b1000110 };

            var results = GetSumCountTuples(marks);

            Console.WriteLine($"Count: { results.count} Sum:{results.sum} average: {results.avarage}");
        }

        private static (int sum, int count,int avarage) GetSumCountTuples(int [] marks)
        {
            var result = (s: 0, c: 0,a:0);

            if (!(marks is null))
            {
                foreach (int mark in marks)
                {
                    Add(mark, 1);
                }
                
                void Add(int s, int c) { result.s += s; result.c += c; result.a = result.s / result.c; };
            }

            return result;
        }
    }
}
